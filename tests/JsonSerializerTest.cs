﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xunit;
using Flagwind.Common;
using Flagwind.Runtime.Serialization;
using Flagwind.ComponentModel;

namespace Flagwind.Externals.Json.Test
{
    public class JsonSerializerTest
    {
        #region 私有变量

        private Flagwind.Security.Credential _credential;

        #endregion

        #region 构造方法

        public JsonSerializerTest()
        {
            var user = new UserProfile(6395335487804014592, "Jason")
            {
                Avatar = "/:mono:/",
                Gender = Gender.Male,
                FullName = "Jason Yang",
                Email = "jasonsoop@gmail.com",
                PhoneNumber = "18682189878",
                Birthdate = new DateTime(1986, 6, 27),
                Grade = 0,
                TotalPoints = RandomGenerator.GenerateInt32(),
                Description = "这个家伙很懒，没来得及留下签名！",
                PrincipalId = "1",
                Principal = new Employee()
                {
                    EmployeeId = 1,
                    EmployeeNo = "A001",
                    Hiredate = new DateTime(2015, 4, 20),
                    UserId = 101,
                }
            };

            ((Employee)user.Principal).User = user;
            _credential = new Security.Credential("20170509", user, "Web", TimeSpan.FromHours(2));
            _credential.ExtendedProperties.Add("QQ", "134****");
            _credential.ExtendedProperties.Add("WeChatNo", "jasonsoop");
            _credential.ExtendedProperties.Add("NativePlace", "湖南娄底");
        }

        #endregion

        #region 测试方法

        /// <summary>
        /// 序列化测试。
        /// </summary>
        [Fact]
        public void SerializeTest()
        {
            JsonSerializer.Default.Settings.Indented = true;

            var text = JsonSerializer.Default.Serialize(new
            {
                UserId = 6395335487804014592,
                Avatar = "/:mono:/",
                Gender = Gender.Male,
                FullName = "Jason",
                Email = "jasonsoop@gmail.com",
                PhoneNumber = "18682189878",
                Birthdate = new DateTime(1986, 6, 27),
                Principal = "001",
                Grade = 0,
                TotalPoints = RandomGenerator.GenerateInt32(),
                Description = "这个家伙很懒，没来得及留下签名！",
            });

            Assert.NotNull(text);

            text = JsonSerializer.Default.Serialize(_credential);

            Assert.NotNull(text);

            JsonSerializer.Default.Settings.NamingConvention = SerializationNamingConvention.Camel;
            text = JsonSerializer.Default.Serialize(_credential);

            Assert.NotNull(text);
        }

        /// <summary>
        /// 反序列化测试。
        /// </summary>
        [Fact]
        public void DeserializeTest()
        {
            JsonSerializer.Default.Settings.Indented = true;
            JsonSerializer.Default.Settings.NamingConvention = SerializationNamingConvention.Camel;
            var text = JsonSerializer.Default.Serialize(_credential);

            Assert.NotNull(text);

            var certification = JsonSerializer.Default.Deserialize<Flagwind.Security.Credential>(text);

            Assert.NotNull(certification);
        }

        /// <summary>
        /// 匿名类型反序列化成字典测试。
        /// </summary>
        [Fact]
        public void DeserializeDictionaryTest()
        {
            var text = @"{AssetId:100001, AssetNo:'A001', Projects:[{ProjectId:1},{ProjectId:3}], Creator:{UserId:100, Name:'Jason'}}";
            var dictionary = JsonSerializer.Default.Deserialize<Dictionary<string, object>>(text);
            Assert.NotNull(dictionary);
            Assert.Equal(4, dictionary.Count);
        }

        #endregion

        #region 测试实体

        public class Employee
        {
            #region 构造函数

            public Employee()
            {
            }

            #endregion

            #region 公共属性

            public long EmployeeId
            {
                get;
                set;
            }

            public string EmployeeNo
            {
                get;
                set;
            }

            public long CorporationId
            {
                get;
                set;
            }

            public byte JobState
            {
                get;
                set;
            }

            public DateTime Hiredate
            {
                get;
                set;
            }

            public DateTime? Leavedate
            {
                get;
                set;
            }

            public int UserId
            {
                get;
                set;
            }

            public UserProfile User
            {
                get;
                set;
            }

            #endregion

            #region 重写方法

            public override bool Equals(object obj)
            {
                if(obj == null || obj.GetType() != this.GetType())
                    return false;

                return ((Employee)obj).EmployeeId == this.EmployeeId;
            }

            public override int GetHashCode()
            {
                return (int)this.EmployeeId;
            }

            #endregion
        }

        public class UserProfile : Flagwind.Security.Membership.User
        {
            #region 构造函数

            public UserProfile(long userId, string name) : base(userId, name)
            {
            }

            public UserProfile(long userId, string name, string @namespace) : base(userId, name, @namespace)
            {
            }

            #endregion

            #region 公共属性

            [SerializationMember("Sex")]
            public Gender Gender
            {
                get;
                set;
            }

            public DateTime? Birthdate
            {
                get;
                set;
            }

            [SerializationMember(SerializationMemberBehavior.Required)]
            public byte Grade
            {
                get;
                set;
            }

            [SerializationMember(SerializationMemberBehavior.Ignored)]
            public int TotalPoints
            {
                get;
                set;
            }

            #endregion

            #region 重写方法

            public override bool Equals(object obj)
            {
                if(obj == null || obj.GetType() != this.GetType())
                    return false;

                var other = (UserProfile)obj;

                return other.UserId == this.UserId && other.Namespace == this.Namespace;
            }

            public override int GetHashCode()
            {
                return (int)this.UserId;
            }

            #endregion
        }

        public enum Gender
        {
            Female = 0,
            Male = 1,
        }

        #endregion
    }
}