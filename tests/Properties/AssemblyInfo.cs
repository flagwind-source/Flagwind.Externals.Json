﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Externals.Json Unit Tests Project")]
[assembly: AssemblyDescription("Flagwind.Externals.Json Unit Tests.")]
[assembly: AssemblyCompany("Flagwind Network")]
[assembly: AssemblyProduct("Flagwind.Externals.Json.Tests")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Network 2010-2016. All rights reserved.")]
[assembly: ComVisible(false)]
[assembly: Guid("a4839198-409c-4d3f-957f-a05770c0be17")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0")]
