﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Externals.Json")]
[assembly: AssemblyDescription("This is a library about JSON serialization based Newtonsoft.Json.")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind.Externals.Json Library")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]
[assembly: ComVisible(false)]
[assembly: Guid("6b576bf0-d3b4-49ba-8b74-6f773a10918b")]
[assembly: AssemblyVersion("1.2.0.509")]
[assembly: AssemblyFileVersion("1.2.0.509")]
